# import time
# import threading
# from tkinter import *
#
# def update_progress_bar(now, all):
#     for percent in range(1, 101):
#         hour = int(percent/3600)
#         minute = int(percent/60) - hour*60
#         second = percent % 60
#
#         x = now / all * 100
#         green_length = int(sum_length * percent / 100)
#         canvas_progress_bar.coords(canvas_shape, (0, 0, green_length, 25))
#         canvas_progress_bar.itemconfig(canvas_text, text='%02d:%02d:%02d' % (hour, minute, second))
#         var_progress_bar_percent.set('%0.2f %%' % percent)
#         time.sleep(0.1)
#
# def run():
#     th = threading.Thread(target=update_progress_bar)
#     th.setDaemon(True)
#     th.start()
#
# top = Tk()
# top.title('Progress Bar')
# top.geometry('800x500+290+100')
# top.resizable(False, False)
# top.config(bg='#535353')
#
# # 进度条
# sum_length = 300
# canvas_progress_bar = Canvas(top, width=sum_length, height=20)
# canvas_shape = canvas_progress_bar.create_rectangle(0, 0, 0, 25, fill='green')
# canvas_text = canvas_progress_bar.create_text(150, 4, anchor=NW)
# canvas_progress_bar.itemconfig(canvas_text, text='00:00:00')
# var_progress_bar_percent = StringVar()
# var_progress_bar_percent.set('00.00 %')
# label_progress_bar_percent = Label(top, textvariable=var_progress_bar_percent, fg='#F5F5F5', bg='#535353')
# canvas_progress_bar.place(relx=0.45, rely=0.4, anchor=CENTER)
# label_progress_bar_percent.place(relx=0.89, rely=0.4, anchor=CENTER)
# # 按钮
# button_start = Button(top, text='开始', fg='#F5F5F5', bg='#7A7A7A', command=run, height=1, width=15, relief=GROOVE, bd=2, activebackground='#F5F5F5', activeforeground='#535353')
# button_start.place(relx=0.45, rely=0.5, anchor=CENTER)
#
# top.mainloop()

from tkinter import *
from tkinter.ttk import *
import time
scale = 100
def running():                   # 开始Progressbar动画
    btn.configure(text="系统忙碌中...",state=DISABLED)
    print("\n"*2)
    print("执行开始".center(scale+28,'-'))
    start = time.perf_counter()
    for i in range(scale+1):
        pb["value"] = i      # 每次更新1
        root.update()            # 更新画面
        a = '*' * i
        b = '.' * (scale - i)
        c = (i/scale)*100
        t = time.perf_counter() - start
        print("\r任务进度:{:>3.0f}% [{}->{}]消耗时间:{:.2f}s".format(c,a,b,t),end="")
        time.sleep(0.03)
    print("\n"+"执行结束".center(scale+28,'-'))
    btn.configure(text="重启任务",state=NORMAL)

root = Tk()
root.geometry("600x500+600+300")
root.title("任务进度可视化")

# 使用默认设置创建进度条
s = Style()
s.theme_use('clam')
# troughcolor:左上栏进度栏最里面的边框
# bordercolor:
s.configure("bar.Horizontal.TProgressbar", troughcolor='#FFFFFF', bordercolor="#FFFFFF", background="pink", lightcolor="#33FF5C", darkcolor="#33FF5C")
pb = Progressbar(root, style="bar.Horizontal.TProgressbar", orient="horizontal", length=600, mode="determinate", maximum=4, value=1)
Label(pb, text='111',background="pink").pack(padx=10,pady=20)
pb.pack(padx=10,pady=20)
pb["maximum"] = 100
pb["value"] = 0


btn = Button(root,text="启动任务",command=running)
btn.pack(pady=10)

root.mainloop()