# -*- coding: utf-8 -*-
from appium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

from appium_demo.init_driver.init_Driver import init_driver

driver = init_driver()


try:
    # 显示等待
    # 超时时间为30s，每隔1s搜索元素是否存在
    # search_button = WebDriverWait(driver, 30, 1).until(lambda x : x.find_elements_by_id("com.android.settings:id/search"))
    print(driver.current_activity)
    print(driver.current_package)
except:
    pass
