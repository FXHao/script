# -*- coding: utf-8 -*-
import json
import time

import requests

class Check_TT():
    def __init__(self):
        # 用户ID
        self.aadvid = 1684316727228487
        # 客户ID（不知道指的是啥，但好像没变过）
        self.customer_id = 1523895
        # 请求头包装
        self.headers = {
            "Host": "datarangers.com.cn",
            "Content-Type": "application/json",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"
        }
        # 请求携带的cookies
        cookies = "passport_csrf_token=1d329b1fe3cea120cd3f9023d3a7752d;" \
                  "passport_csrf_token_default=1d329b1fe3cea120cd3f9023d3a7752d;" \
                  "sid_guard=3972d16a6904daf351360f4a931ca614%7C1612410706%7C5184000%7CMon%2C+05-Apr-2021+03%3A51%3A46+GMT;" \
                  "uid_tt=65ebda3bad161b3f8bf96b123008218a;" \
                  "uid_tt_ss=65ebda3bad161b3f8bf96b123008218a;" \
                  "sid_tt=3972d16a6904daf351360f4a931ca614;" \
                  "sessionid=3972d16a6904daf351360f4a931ca614;" \
                  "sessionid_ss=3972d16a6904daf351360f4a931ca614;" \
                  "lang=zh_CN;" \
                  "SameSite=None"
        self.cookies = {i.split("=")[0]: i.split("=")[1] for i in cookies.split(";")}

        # 需检测的游戏id
        # self.appid = 218919
        self.appid = None
        # 设备id
        self.device_id = None

    def user_input(self):
        for x in range(3):
            input_appid = input('请输入游戏的【Appid】,回车键确认:\n')
            if input_appid != '':
                self.appid = int(input_appid)
                break

        for x in range(2):
            input_did = input('请输入手机对应【driver_id】,回车键确认:\n')
            if input_did != '':
                self.device_id = input_did
                break
            # print(self.device_id)

        # print('driver_id为【{}】'.format(self.device_id))

    def get_result_id(self, appid, device_id):
        """
        获取result_id
        :param appid: 游戏id
        :param device_id: 设备id
        :return: result_id
        :{"code":0,"data":{"result_id":"133208"},"message":"Success"}
        """
        # 获取result——id的接口// POST
        result_id_url = "https://datarangers.com.cn/checker/api/v1/sdk_check/auto_check?aadvid=1684316727228487&customer_id=1523895&industry=%E6%B8%B8%E6%88%8F&referer=ad"
        result_id = None
        payloadData  = {
            "device_id": device_id,
            "app_id": appid
        }
        paydata = json.dumps(payloadData)
        # print(paydata)
        respon = requests.post(result_id_url, data=paydata, headers=self.headers, cookies=self.cookies)
        if respon.json()['message'] == '参数异常':
            print("获取result_id错误，可能设备ID填写错误~")
            return result_id
        result_id = respon.json()['data']['result_id']
        return result_id

    def get_check_json(self, result_id):
        # 获取检测结果的接口// GET
        check_url = "https://datarangers.com.cn/checker/api/v1/sdk_check/auto_check_result?aadvid={}&customer_id={}&industry=%E6%B8%B8%E6%88%8F&referer=ad&app_id={}&result_id={}&index=1".format(self.aadvid, self.customer_id, self.appid, result_id)
        respon = requests.get(check_url, headers=self.headers, cookies=self.cookies).json()
        respon_json = respon['data']
        # print(respon_json)

        basic_info = respon_json['basic_info']['message'] # 基本信息
        b_message_detail = respon_json['basic_info']['message_detail']
        try:
            app_package = respon_json['basic_info']['result'][0]['ok_logs']['header']["app_package"]
            user_agent = respon_json['basic_info']['result'][0]['ok_logs']['header']['user_agent']
            datetime = respon_json['basic_info']['result'][0]['ok_logs']['datetime']
        except:

            app_package = None
            user_agent = None
            datetime = None

        if datetime is not None:
            tiemArray = time.localtime(datetime)
            datetime = time.strftime("%Y-%m-%d %H:%M:%S", tiemArray)
        # print(datetime)
        # print(user_agent)

        device_check = respon_json['device']['message']  # 设备
        d_message_detail = respon_json['device']['message_detail']
        duration_check = respon_json['duration']['message'] # 时长
        duration_message_detail = respon_json['duration']['message_detail']
        purchase_check = respon_json['purchase']['message'] # 支付
        purchase_message_detail = respon_json['purchase']['message_detail']
        register_check = respon_json['register']['message'] # 注册
        register_message_detail = respon_json['register']['message_detail']

        data = """
>>>>>>>>>>>>基础信息<<<<<<<<<<<<\n
时间：【{}】
设备信息：【{}】
包名：【{}】
appid：【{}】
device_id：【{}】\n
>>>>>>>>>>>>检测结果<<<<<<<<<<<<\n
基本信息：【{}】
   info：{}\n
设备激活：【{}】
   info：{}\n
时长统计：【{}】
   info: {}\n
注册事假：【{}】
   info：{}\n
购买事件：【{}】
   info：{}\n
            """.format(datetime, user_agent, app_package, self.appid, self.device_id, basic_info, b_message_detail, device_check, d_message_detail, duration_check, duration_message_detail, register_check, register_message_detail, purchase_check, purchase_message_detail)
        print(data)

    def main(self):
        self.user_input()
        print("\n开始检测>>>>\n")
        if self.appid is None:
            print("appid为空，检测失败！")
            return
        try:
            result_id = self.get_result_id(self.appid, self.device_id)
        except:
            print("检测失败，请检查appid后重新运行！")
            return
        if result_id == None:
            return
        print("成功获取resul_id:{}".format(result_id))
        self.get_check_json(result_id)


if __name__ == '__main__':
    res = Check_TT()
    res.main()
    while True:
        input()
        