import tkinter as tk
import tkinter.ttk as ttk
from  tkinter import filedialog, messagebox
from tkinter import scrolledtext

import pymysql
import csv
import os
import subprocess
import re
import zipfile
import hashlib
import time

class Tools_UI():
    def __init__(self):

        self.mode = 0 # 默认0（测试）
        self.path_ = None
        self.debug_num = 0
        self.rc_num = 0
        self.r_mysql = Read_mysql()
        self.apk = GetApkInfo()
        self.py_path = os.getcwd()
        self.init_UI()

    def init_UI(self):
        self.root = tk.Tk()
        self.root.title('本地化apk检测工具v1.0')
        self.root.iconbitmap(os.path.join(self.py_path, 'MTT.ico'))
        ws = self.root.winfo_screenwidth()
        hs = self.root.winfo_screenheight()
        self.root.geometry('630x530+%d+%d' % ((ws/2)-530/2, (hs/2)-600/2))
        self.root.configure(background="pink")

        # 路径获取
        La1= tk.Label(self.root, text='选择路径:', font=('微软雅黑', 12), background="pink")
        La1.place(x=10, y=10,width=100, height=25)
        self.path_p = tk.StringVar()

        #输入框绑定变量path
        En1 = tk.Entry(self.root, textvariable = self.path_p)
        En1.place(x=120, y=10, width=300, height=25)
        B1 = tk.Button(self.root, text="选择", command=self.selectpath, font=('微软雅黑', 11), background="pink")
        B1.place(x=450, y=10, width=60, height=25)

        # 功能复选
        La2 = tk.Label(self.root, text='模式选择:', font=('微软雅黑', 12), background="pink")
        La2.place(x=10, y=55, width=100, height=25)
        self.CheckVar1 = tk.IntVar()
        R1 = tk.Radiobutton(self.root,text='测试(仅检测)',variable=self.CheckVar1, value=0, command=self.selection, background="pink")
        R2 = tk.Radiobutton(self.root,text='提审(检测+写入表格)',variable=self.CheckVar1, value=1, command=self.selection,background="pink")
        R1.place(x=30, y=90)
        R2.place(x=160, y=90)

        La3 = tk.Label(self.root, text='结果显示:', font=('微软雅黑', 12), background="pink")
        La3.place(x=15, y=130)

        # 滚动条文本框
        self.Text1 = tk.scrolledtext.ScrolledText(self.root, background="pink", relief="solid", font=('微软雅黑', 10))
        self.Text1.place(x=10, y=200, height=300, width=600)
        self.Text1.config(state=tk.DISABLED)

        # 运行按钮
        self.B2 = tk.Button(self.root, text='运 行', font=('微软雅黑', 13), command=self.run,background='#32CD32')
        self.B2.place(x=450, y=70, height=80, width=80)

        # 进度条
        s = ttk.Style()
        s.theme_use('clam')
        s.configure("bar.Horizontal.TProgressbar", troughcolor='#FFFFFF', bordercolor="#FFFFFF", background="#63BAD0", lightcolor="pink", darkcolor="pink")
        self.Prb = ttk.Progressbar(self.root, style="bar.Horizontal.TProgressbar", orient="horizontal", length=300,mode="determinate", maximum=4, value=1)
        self.Prb.place(x=25, y=160)
        self.Prb["maximum"] = 0
        self.Prb["value"] = 0

        # 进度百分比
        self.text1 = tk.StringVar()
        self.La4 = tk.Label(self.root, textvariable=self.text1, background="#ffffff")

        # 时间
        self.text2 = tk.StringVar()
        self.La5 = tk.Label(self.root, textvariable=self.text2, background="#ffffff")

        self.root.mainloop()

    def progressbar_updat(self):
        self.Prb["value"] += 1
        self.text1.set('{:>3.0f} %'.format((self.Prb["value"]/self.Prb["maximum"])*100))
        self.root.update()

    def selectpath(self):
        self.path_ = filedialog.askdirectory()
        # path_ = path_.replace('/', '\\\\')
        self.path_p.set(self.path_)

    def selection(self):
        self.mode = self.CheckVar1.get()

    def logset(self, data):
        self.Text1.config(state=tk.NORMAL)
        self.Text1.insert('end', data + '\n')
        self.Text1.update()
        self.Text1.config(state=tk.DISABLED)

    def write_csv(self):
        with open(os.path.join(self.path_, "getmd5.csv"), "w") as self.csv:
            self.csv.write("{0},{1},{2},{3},{4},{5},{6},{7}\n".format("游戏", "更新内容", "渠道", "版本", "内部版本号", "文件名", "包名","MD5"))
            self.chack_apk()

    def chack_apk(self):
        if (self.path_ is not None) and (self.path_ != ''):
            for apk in os.listdir(self.path_):
                self.progressbar_updat()
                if os.path.splitext(apk)[1] != ".apk":
                    continue
                apk_p = self.path_ + '\\' + apk
                self.rc_num += 1
                file = open(apk_p, 'rb')
                md5 = hashlib.md5(file.read()).hexdigest()

                # 实际信息
                packagename, versionCode, versionName, permission_list, g_name_cn, targetSDK = self.apk.getAppBaseInfo(apk_p)
                gamename = apk.split("-")[0]
                # print(gamename)
                channel = apk.split("-")[1]
                appid = int(self.apk.get_TrackSdkConfig(apk_p, 'app_id'))
                platformid = int(self.apk.get_TrackSdkConfig(apk_p, 'platform_id'))

                # 数据库信息
                try:
                    g_mininame_sql, appid_sql, g_name_cn_sql = self.r_mysql.select_Game(gamename)
                except:
                    self.logset("【{}】查询数据库Game表失败".format(apk))
                    continue
                try:
                    c_name_cn_sql, platformid_sql, permissions_li_sql = self.r_mysql.select_Channel(channel)
                except:
                    self.logset("【{}】查询数据库Channel表失败".format(apk))
                    continue
                try:
                    packagename_sql, targetsdk_sql, versioncode_sql= self.r_mysql.select_Packagename(channel, g_mininame_sql)
                except:
                    self.logset("【{}】查询数据库Packagename失败".format(apk))
                    continue

                # 校验正式包和debug包
                if (apk.split("-")[2] == 'debug') and (self.mode == 1):
                    self.debug_num += 1
                    self.logset('【{}-{}】为debug包，提审请注意！！'.format(g_mininame_sql, channel))

                # 校验是否含需禁权限
                if permissions_li_sql is not None:
                    for i in permissions_li_sql:
                        if i in permission_list:
                            self.logset("【{}-{}】有需禁权限{}，请删除！".format(g_mininame_sql, channel, i))

                # 校验appid和platform_id
                if (appid_sql != None) or (platformid_sql != None):
                    if appid != appid_sql:
                        self.logset("【{}-{}】的appid为{}，与数据库不符，请检查！".format(g_mininame_sql, channel, appid))
                    if platformid != platformid_sql:
                        self.logset("【{}-{}】的渠道id为{}，与数据库不符，请检查！".format(g_mininame_sql, channel, platformid))
                else:
                    self.logset("【{}-{}】在数据库中未查询到appid和渠道id，请先设置！".format(g_mininame_sql, channel))

                # 校验包名
                if packagename_sql is not None:
                    if packagename_sql != packagename:
                        self.logset("【{}-{}】包名为 {}，与数据库中不一致，请检查！".format(g_mininame_sql, channel, packagename))
                else:
                    self.logset("【{}-{}】在数据库中未查询到包名，请先设置！".format(g_mininame_sql, channel))

                # 校验中文游戏名
                if g_name_cn_sql is not None:
                    if g_name_cn_sql != g_name_cn:
                        self.logset("【{}-{}】中文游戏名为:{}，与数据库中不符，请检查！".format(g_mininame_sql, channel, g_name_cn))

                # 校验targetSDK
                if targetsdk_sql is not None:
                    if targetSDK < targetsdk_sql:
                        self.logset("【{}-{}】TargetSDK为{}，小于数据库中信息，请检查！".format(g_mininame_sql, channel, targetSDK))
                    if targetSDK > targetsdk_sql:
                        self.logset("【{}-{}】TargetSDK提升到了{}，请注意~".format(g_mininame_sql, channel, targetSDK))
                        if self.mode == 1:
                            self.r_mysql.update_P(channel, g_mininame_sql, 'targetsdk', targetSDK)
                else:
                    if self.mode == 1:
                        self.r_mysql.update_P(channel, g_mininame_sql, 'targetsdk', targetSDK)

                # 校验版本号
                if versioncode_sql is not None:
                    if versionCode <= versioncode_sql:
                        self.logset("【{}-{}】版本号为{}，<= 数据库中信息，请检查！".format(g_mininame_sql, channel, versionCode))
                    if (versionCode > versioncode_sql) and (self.mode == 1):
                        self.r_mysql.update_P(channel, g_mininame_sql, 'versioncode', versionCode)
                else:
                    if self.mode == 1:
                        self.r_mysql.update_P(channel, g_mininame_sql, 'versioncode', versionCode)

                self.logset("{}--{} 检测完成~".format(g_mininame_sql, channel))

                if self.mode == 1:
                    self.csv.write("{0},{1},{2},{3},{4},{5},{6},{7}\n".format(g_mininame_sql, '',c_name_cn_sql, versionName, versionCode, apk, packagename, md5))
                    self.logset("{}--{} 写入完成~".format(g_mininame_sql, channel))

                t = time.perf_counter() - self.start
                self.text2.set('{:.2f} s'.format(t))
                # self.root.update()

    def run(self):
        self.B2.configure(text='运行中..', state=tk.DISABLED)
        self.debug_num = 0
        self.rc_num = 0

        if self.mode == 0:
            m = '测试模式'
        elif self.mode == 1:
            m = '提审模式'
        path = '文件路径：%s\n' % self.path_
        mode = '执行模式：%s\n' % m
        datatext = path + mode +'============================\n'

        if (self.path_ is not None) and (self.path_ != ''):
            self.Prb["maximum"] = len(os.listdir(self.path_))
            self.Prb["value"] = 0
            self.La4.place(x=350, y=135, height=25, width=60)
            self.La5.place(x=350, y=165, height=25, width=60)

        self.Text1.config(state=tk.NORMAL)
        self.Text1.delete(1.0,'end')
        self.Text1.insert('insert', datatext)
        self.Text1.config(state=tk.DISABLED)

        self.start = time.perf_counter()
        if self.mode == 0:
            self.chack_apk()
        elif self.mode == 1:
            self.write_csv()
        messagebox.showinfo(title='重要提醒！', message='检测完成\n记得写提包时间！\n记得写提包时间！\n记得转单！\n记得转单！')
        self.logset('============================')
        self.logset("总包数：{}\ndebug包数：{}".format(self.rc_num, self.debug_num))

        self.B2.configure(text='重新运行', state=tk.NORMAL)


class Read_mysql():
    def __init__(self, sql=None):
        self.conn = pymysql.connect(host='139.9.43.7',port=3306,user='root',password='haohao520',database='JK_apkinfo',charset='utf8')
        self.cursor = self.conn.cursor()
        self.sql = sql

    def __del__(self):
        self.cursor.close()
        self.conn.close()

    def read_Channel(self):
        '''
        读取Channel全表信息
        :return:
        '''
        self.sql = 'SELECT * FROM Channel'
        self.cursor.execute(self.sql)
        data = self.cursor.fetchall()
        for item in data:
            print('{str1: <{len}} {str2: <{len}} {str3: <{len}}'
                  .format(str1=item[0],str2=item[1], str3=item[2], len=8))

    def insert_Channel(self):
        '''
        将渠道信息写入数据库
        :return:
        '''
        for i in channerdict:
            v = channerdict[i]
            channel_name = i
            channel_name_cn = v[0]
            if v[2] is None:
                v[2] = 0
            channel_id = v[2]
            self.sql = """INSERT INTO Channel(channel_name, channel_name_cn, channel_id) VALUES('%s','%s',%d)""" % (channel_name, channel_name_cn, channel_id)
            self.cursor.execute(self.sql)
        self.conn.commit()

    def insert_Game(self):
        '''
        写入游戏信息写入数据库
        :return:
        '''
        for i in gamenamedict:
            v = gamenamedict[i]
            game_name = i
            game_name_abbreviation =v[0]
            app_id = v[2]
            game_name_cn = v[3]
            self.sql = """INSERT INTO Game(game_name, game_name_abbreviation, app_id, game_name_cn) VALUES('%s','%s','%d','%s')""" % (game_name, game_name_abbreviation, app_id, game_name_cn)
            self.cursor.execute(self.sql)
        self.conn.commit()

    def insert_P(self):
        with open('P.csv', 'r') as p:
            data = csv.reader(p)

            for i in data:
                self.sql = """INSERT INTO Packagename(channel_name, game_name_abbreviation, packagename) VALUES('%s','%s','%s')""" % (i[0], i[1], i[2])
                self.cursor.execute(self.sql)
            self.conn.commit()

    def select_Channel(self, channel_name):
        """
        获取渠道名、渠道id、权限
        :param channel_name:
        :return:
        """
        ensitive_permissions_li = None
        try:
            self.sql = """SELECT * FROM Channel WHERE channel_name='{}'""".format(channel_name)
            self.cursor.execute(self.sql)
            data = self.cursor.fetchone()
            channel_name_cn = data[1]
            channel_id = data[2]
            ensitive_permissions = data[3]
            if ensitive_permissions != None:
                ensitive_permissions_li = ensitive_permissions.split(',')
        except:
            return None
        return channel_name_cn, channel_id, ensitive_permissions_li

    def select_Game(self, game_name):
        """
        获取游戏简写名、appid、游戏中文名
        :param game_name:
        :return:
        """
        try:
            self.sql = """SELECT * FROM Game WHERE game_name='{}'""".format(game_name)
            self.cursor.execute(self.sql)
            data = self.cursor.fetchone()
            game_name_abbreviation = data[1]
            app_id = data[2]
            game_name_cn = data[3]
        except:
            return None
        return game_name_abbreviation, app_id, game_name_cn

    def select_Packagename(self, channel_name, game_name_abbreviation):
        """
        获取包名、targetsdk、versioncode
        :param channel_name:
        :param game_name_abbreviation:
        :return:
        """
        try:
            # print(channel_name)
            # print(game_name_abbreviation)
            self.sql = """SELECT packagename, targetsdk, versioncode FROM Packagename WHERE channel_name = '{0}' AND game_name_abbreviation = '{1}'""".format(channel_name, game_name_abbreviation)
            self.cursor.execute(self.sql)
            data = self.cursor.fetchone()
            packagename = data[0]
            targetsdk = data[1]
            versioncode = data[2]

        except Exception as e:
            return None
        return packagename, targetsdk, versioncode

    def update_P(self, channel_name, game_name_abbreviation, key, value):

        try:
            self.sql = """UPDATE Packagename SET {}={} WHERE channel_name='{}' AND game_name_abbreviation='{}'""".format(key,value, channel_name, game_name_abbreviation)
            self.cursor.execute(self.sql)
            self.conn.commit()
            return True
        except:
            self.conn.rollback()
            return False


class GetApkInfo():
    def __init__(self):
        '''
        :param path: 获取到的路径
        '''
        path = os.getcwd()
        self.aapt_path = os.path.join(path,'aapt.exe')
        # self.jk_sql = Read_mysql()

    def getAppBaseInfo(self, apk_path):
        '''
        获取apk包名，版号，内部版本号，权限信息
        :param apk_path: apk文件
        :return: packagename, versionCode, versionName, compileSdkVersion
        '''
        get_info_command = "%s dump badging %s" % (self.aapt_path, apk_path)
        # print(get_info_command)
        info_output = subprocess.Popen(get_info_command, shell=True, stdout=subprocess.PIPE)
        info = info_output.stdout.read().decode('utf8', 'ignore')
        match = re.compile("package: name='(\S+)' versionCode='(\d+)' versionName='(\S+)' compileSdkVersion='(\d+)'").match(info)
        # print(info)
        if not match:
            raise Exception("{}找不到apk版号信息~".format(apk_path))

        g_name = re.search("application-label-zh:'(\S+)'", info)
        # print(g_name is None)
        if g_name is None:
            g_name = re.search("application-label-zh-CN:'(\S+)'", info)
        elif g_name is None:
            g_name = re.search("application-label-zh-Hans-CN:'(\S+)'", info)
        elif g_name is None:
            g_name = re.search("application-label-en:'(\S+)'", info)
        elif g_name is None:
            raise Exception("{}找不到apk中文名信息~".format(apk_path))

        t_sdk = re.search("targetSdkVersion:'(\d+)'", info)
        # print(g_name.group(1))
        packagename = match.group(1)
        versionCode = match.group(2)
        versionName = match.group(3)
        targetSDK = t_sdk.group(1)
        # print(targetSDK)
        apk_game_name = g_name.group(1)
        # print(game_name)

        # 获取权限目录
        get_permission = "%s d permissions %s" % (self.aapt_path, apk_path)
        p_output = subprocess.Popen(get_permission, shell=True, stdout=subprocess.PIPE)
        p_data = p_output.stdout.read().decode('utf8', 'ignore')
        p_data_list = p_data.split('\n')
        permission_list = []
        for i in p_data_list:
            if i is '':
                continue
            permission_list.append(i.split()[1])

        return packagename, versionCode, versionName, permission_list, apk_game_name, targetSDK

    def get_TrackSdkConfig(self, apk_path, key):
        '''
        获取TrackSdk配置信息(appid和渠道id)
        :param key: 需获取的参数
        :return: 获取参数的值
        '''
        with zipfile.ZipFile(apk_path, 'r') as apkzip:
            data = apkzip.read('assets/TrackSdkConfig.properties')
            try:
                data = str(data, encoding='gbk')
            except Exception as e:
                data = str(data, encoding='utf8')
            properties_dict = {}
            line_list = data.split('\n')

            for line in line_list:
                if line.startswith('#') or (line is '\r') or (line is ''):
                    continue
                # k, v = line.split('=')
                kv = line.split('=')
                if len(kv) < 2:
                    continue
                k = kv[0].strip()
                v = kv[1].strip()

                properties_dict[k] = v
        if key in properties_dict:
            return properties_dict[key]
        return "未找到{}".format(key)

if __name__ == '__main__':
    # Tools_UI()
    # print(os.getcwd())
    A = Read_mysql()
    a = A.select_Channel('huawei')[2]
    print(a)
    print('android.permission.SEND_SMS' in a)
    B = GetApkInfo()
    b = B.getAppBaseInfo("D:\\Users\\Desktop\\安卓自动化\\gitlab-script\\Py脚本\\apkcheck\\TalkingTom2-huawei-huaweiad-1024-rc-b574-v5.3.15.574.apk")[3]
    print(b)
    for i in b:
        if i in a:
            print(i)

