# -*- coding: utf-8 -*-
import pymysql
import csv
import os
import subprocess
import re
import zipfile
import hashlib
from tools_ui import Tools_UI


class Read_mysql():
    def __init__(self, sql=None):
        self.conn = pymysql.connect(host='139.9.43.7',port=3306,user='root',password='haohao520',database='JK_apkinfo',charset='utf8')
        self.cursor = self.conn.cursor()
        self.sql = sql

    def __del__(self):
        self.cursor.close()
        self.conn.close()

    def read_Channel(self):
        '''
        读取Channel全表信息
        :return:
        '''
        self.sql = 'SELECT * FROM Channel'
        self.cursor.execute(self.sql)
        data = self.cursor.fetchall()
        for item in data:
            print('{str1: <{len}} {str2: <{len}} {str3: <{len}}'
            .format(str1=item[0],str2=item[1], str3=item[2], len=8))

    def insert_Channel(self):
        '''
        将渠道信息写入数据库
        :return:
        '''
        for i in channerdict:
            v = channerdict[i]
            channel_name = i
            channel_name_cn = v[0]
            if v[2] is None:
                v[2] = 0
            channel_id = v[2]
            self.sql = """INSERT INTO Channel(channel_name, channel_name_cn, channel_id) VALUES('%s','%s',%d)""" % (channel_name, channel_name_cn, channel_id)
            self.cursor.execute(self.sql)
        self.conn.commit()

    def insert_Game(self):
        '''
        写入游戏信息写入数据库
        :return:
        '''
        for i in gamenamedict:
            v = gamenamedict[i]
            game_name = i
            game_name_abbreviation =v[0]
            app_id = v[2]
            game_name_cn = v[3]
            self.sql = """INSERT INTO Game(game_name, game_name_abbreviation, app_id, game_name_cn) VALUES('%s','%s','%d','%s')""" % (game_name, game_name_abbreviation, app_id, game_name_cn)
            self.cursor.execute(self.sql)
        self.conn.commit()

    def insert_P(self):
        with open('P.csv', 'r') as p:
            data = csv.reader(p)

            for i in data:
                self.sql = """INSERT INTO Packagename(channel_name, game_name_abbreviation, packagename) VALUES('%s','%s','%s')""" % (i[0], i[1], i[2])
                self.cursor.execute(self.sql)
            self.conn.commit()

    def select_Channel(self, channel_name):
        """
        获取渠道名、渠道id、权限
        :param channel_name:
        :return:
        """
        try:
            self.sql = """SELECT * FROM Channel WHERE channel_name='{}'""".format(channel_name)
            self.cursor.execute(self.sql)
            data = self.cursor.fetchone()
            channel_name_cn = data[1]
            channel_id = data[2]
            ensitive_permissions = data[3]
        except:
            return None
        return channel_name_cn, channel_id, ensitive_permissions

    def select_Game(self, game_name):
        """
        获取游戏简写名、appid、游戏中文名
        :param game_name:
        :return:
        """
        try:
            self.sql = """SELECT * FROM Game WHERE game_name='{}'""".format(game_name)
            self.cursor.execute(self.sql)
            data = self.cursor.fetchone()
            game_name_abbreviation = data[1]
            app_id = data[2]
            game_name_cn = data[3]
        except:
            return None
        return game_name_abbreviation, app_id, game_name_cn

    def select_Packagename(self, channel_name, game_name_abbreviation):
        """
        获取包名、targetsdk、versioncode
        :param channel_name:
        :param game_name_abbreviation:
        :return:
        """
        try:
            self.sql = """SELECT packagename, targetsdk, versioncode FROM Packagename WHERE channel_name = '{0}' AND game_name_abbreviation = '{1}'""".format(channel_name, game_name_abbreviation)
            self.cursor.execute(self.sql)
            data = self.cursor.fetchone()
            packagename = data[0]
            targetsdk = data[1]
            versioncode = data[2]
        except:
            return None
        return packagename, targetsdk, versioncode


class Checkapk():
    def __init__(self, path):
        '''
        :param path: 获取到的路径
        '''
        self.path = path
        self.debug_num = 0
        self.apk_path_list = os.listdir(path)
        self.aapt_path = 'aapt.exe'
        self.jk_sql = Read_mysql()
        self.ui = Tools_UI()


    def getAppBaseInfo(self, apk_path):
        '''
        获取apk包名，版号，内部版本号，权限信息
        :param apk_path: apk文件
        :return: packagename, versionCode, versionName, compileSdkVersion
        '''
        get_info_command = "%s dump badging %s" % (self.aapt_path, apk_path)
        info_output = subprocess.Popen(get_info_command, shell=True, stdout=subprocess.PIPE)
        info = info_output.stdout.read().decode('utf8', 'ignore')
        match = re.compile("package: name='(\S+)' versionCode='(\d+)' versionName='(\S+)' compileSdkVersion='(\d+)'").match(info)
        if not match:
            raise Exception("{}找不到apk版号信息~".format(apk_path))

        g_name = re.search("application-label-zh:'(\S+)'", info)
        # print(g_name is None)
        if g_name is None:
            g_name = re.search("application-label-zh-CN:'(\S+)'", info)
        elif g_name is None:
            g_name = re.search("application-label-zh-Hans-CN:'(\S+)'", info)
        elif g_name is None:
            g_name = re.search("application-label-en:'(\S+)'", info)
        elif g_name is None:
            raise Exception("{}找不到apk中文名信息~".format(apk_path))
        # print(g_name.group(1))
        packagename = match.group(1)
        versionCode = match.group(2)
        versionName = match.group(3)
        targetSDK = match.group(4)
        apk_game_name = g_name.group(1)
        # print(game_name)

        # 获取权限目录
        get_permission = "%s d permissions %s" % (self.aapt_path, apk_path)
        p_output = subprocess.Popen(get_permission, shell=True, stdout=subprocess.PIPE)
        p_data = p_output.stdout.read().decode('utf8', 'ignore')
        p_data_list = p_data.split('\n')
        permission_list = []
        for i in p_data_list:
            if i is '':
                continue
            permission_list.append(i.split()[1])

        return packagename, versionCode, versionName, permission_list, apk_game_name, targetSDK

    def get_TrackSdkConfig(self, apk_path, key):
        '''
        获取TrackSdk配置信息(appid和渠道id)
        :param key: 需获取的参数
        :return: 获取参数的值
        '''
        with zipfile.ZipFile(apk_path, 'r') as apkzip:
            data = apkzip.read('assets/TrackSdkConfig.properties')
            try:
                data = str(data, encoding='gbk')
            except Exception as e:
                data = str(data, encoding='utf8')
            properties_dict = {}
            line_list = data.split('\n')

            for line in line_list:
                if line.startswith('#') or (line is '\r') or (line is ''):
                    continue
                # k, v = line.split('=')
                kv = line.split('=')
                if len(kv) < 2:
                    continue
                k = kv[0].strip()
                v = kv[1].strip()

                properties_dict[k] = v
        if key in properties_dict:
            return properties_dict[key]
        return "未找到{}".format(key)

    def apkcheck(self, apk):
        # 获取MD5码
        # file = open(os.path.join(self.path, apk), 'rb')
        # md5 = hashlib.md5(file.read()).hexdigest()

        if apk.split('-')[2] == 'debug':
            # if self.ui.mode == 1:
            self.debug_num += 1
            self.ui.Text1.insert('end', '【{}】未非正式提审包请注意！！'.format(apk))
            self.ui.Text1.update()

        # gamename = apk.split("-")[0]
        # channel = apk.split("-")[1]
        # packagename, versionCode, versionName, permission_list, apk_game_name, targetSDK = self.getAppBaseInfo(apk)




# A = Read_mysql()
# b = A.select_Packagename('baiduone', 'MTT')
# print(b)




