# -*- encoding=utf8 -*-
import hashlib
import os
import re
import subprocess
import xlrd
import zipfile
import time


data = []
file_path = []
num = 0
def re_xsl():
    worksheet = xlrd.open_workbook("hash.xlsx")
    sheet = worksheet.sheet_by_name("Sheet1")
    rows = sheet.nrows
    # cols = sheet.ncols
    # cols = sheet.col_values(2)
    for i in range(rows):
        cell = sheet.cell_value(i,0)
        data.append(cell)
    # print(data)

def get_file(path):
    for i in os.listdir(path):
        newdir = os.path.join(path, i)
        if os.path.isfile(newdir):
            if os.path.splitext(i)[1] == ".apk":
                global num
                num += 1
                file_path.append(newdir)
        elif os.path.isdir(newdir): # 递归
            get_file(newdir)



def get_md5():
    print("========================== 开始获取MD5...... =======================")

    with open(os.path.join("getmd5.csv"), "w") as csv:
        csv.write("{0},{1}\n".format("游戏", "MD5"))
        for i in file_path:
            file = open(i, 'rb')
            md5 = hashlib.md5(file.read()).hexdigest()
            print(md5)
            if md5 in data:
                print(i)
                csv.write("{0},{1}\n".format(i,md5))
            # csv.write("{0},{1}\n".format(i,md5))


if __name__ == "__main__":
    dir = os.getcwd()
    get_file(dir)
    print(file_path)
    re_xsl()
    print(data)
    get_md5()
    print(num)
    print("========================== 读写MD5完成 ==========================")

