# -*- coding: utf-8 -*-
import os

from appium import webdriver

def init_driver():
    desired_caps = {
        "platformName": "Android",              # 平台的名称：iOS, Android, or FirefoxOS
        "platformVersion": '',                  # 设备系统版本号
        "deviceName": str(os.system("adb devices")),       # 设备号 IOS：instruments -s devices，Android: adb devices
        #"app": "",                              # 安装文件路径：/abs/path/to/my.apk or http://myapp.com/app
        # "appActivity": ".ui.activity.BrowserActivity",                      # 启动的Activity
        # "appPackage": "mark.via",                       # 启动的包
        "unicodeKeyboard": True,                  # unicode设置(允许中文输入)
        "resetKeyboard": True                     # 键盘设置(允许中文输入)
    }
    driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    return driver