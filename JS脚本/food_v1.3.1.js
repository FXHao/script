/**
 *  editor：fxhao
 *  createDate：2020-12-25
 *  description：悠悠团餐抢餐
 *  version: 1.3.2
**/
var name_list = [];  // 存放已点击的送餐人姓名
var food_num_select = 3; // 需要抢餐的数量
var receive_num = 0;  // 已领取餐品的数量
var food_name_list = []; // 存放餐名关键词

// 检测无障碍
function OpenPermissions() {
    var quanxian = auto.service;
    console.log('【正在检测无障碍服务...】');
    if (quanxian == null) {
        console.error('请手动为Auto.js软件开启无障碍服务~');
        auto.waitFor('fast');
    } else {
        console.info('无障碍服务已开启~');
    };
};

// 启动微信
function startapp() {
    var pn = '微信'
    packname = app.getPackageName(pn);
    if (packname != null) {
        // console.log('【正在启动 %s ...】', pn)
        app.launch(packname);
    } else {
        alert('未找到%s', pn);
    }
    dialogs.alert("请手动打开微信且进入大群~\n脚本将在5秒后执行~");
};

/*
选择需要抢餐的份数
food_num_select = 0 ===> 1份
                  1 ===> 2份
                  3 ===> 不限份数
*/
function selectfoodnum() {
    var food_num_list = ['一份', '两份', '有多少来多少（慎点！！小心被打）'];
    var cancel_text = ["人生中那么选择，请认真对待~", '还不认真点？', '有按钮你非得点空白的地方？', "很好玩吗？","确定都不会点？"]
    while(true){
        var i = dialogs.singleChoice('请选择需要抢餐的份数：',food_num_list);
        switch (i) {
            case -1:
                toast(cancel_text[random(0,4)]);
                continue;
            case 0:
                food_num_select = 1;
                return;
            case 1:
                food_num_select = 2;
                return;
            case 2:
                alert("真是只猪!");
                food_num_select = 9999;
                return;
        };
    };
};

// 获取餐名关键词
function foodnameinput() {
    while (true) {
        var f_n_i = dialogs.rawInput('请输入抢餐的关键词：\n1、如【凉瓜滑蛋饭】可输入【凉瓜】或者【滑蛋饭】(需餐名包含) \n2、支持多样选择，请以逗号(，)分割，如【水果，寿司】\n3、不填为默认全选', '水果');
        switch (f_n_i) {
            case null:
                toast('请点击确定按钮');
                continue;
            default:
                food_name_list = f_n_i.split(/[,，]/);
                console.info('抢餐列表为：%s', food_name_list);
                return;
        };
    };
};

// 判断data是否在数组中存在
function isIndict(dict, data){
    /*判断data是否在数组中存在 */
    for(var i=0; i<dict.length; i++){
        if (dict[i] == data) {
            return true;
        }};
    return false;
};

// 去除数组指定元素
// list = list.filter(function (data) {
    //     return data != null;
    // })
    // log(list.length);

// 判断是否需为需要的餐品
function isReceive() {
    // log(food_name_list)
    for (var j = 0; j < food_name_list.length; j++) {
        if (food_name_list[0] == '') {
            return true;
        };
        var x = textContains(food_name_list[j]).exists();
        if (x) {
            return true;
        };
    };
    return false;
};


function food() {
    // 获取送餐列表
    var food_list = textContains("送给你一份美味的餐品啦").find();
    // log(food_list.length)
    for (var i = 0; i < food_list.length; i++){
        // 获取送餐人姓名
        var name  = food_list[food_list.length - i - 1].text().split('送')[0];
        if (isIndict(name_list, name)) {
            continue;
        };
        // 将姓名加入送餐人列表
        name_list.push(name);
        
        // 获取送餐链接可点击的控件
        // sleep(400);
        textContains("送给你一份美味的餐品啦").waitFor()
        var food_bu = food_list[food_list.length - i - 1].parent();
        if (food_bu == null) {
            continue;
        };
        console.time('耗时')
        console.verbose('发现一个新的链接~');
        
        // 点击送餐链接
        food_bu.click();
        
        // 等待进入领取页面
        textContains("取餐时段").waitFor();
        // sleep(400);

        if (!isReceive()) {
            console.log('【该餐品不在抢餐列表里面】')
            desc('返回').findOne().parent().click();
            continue;
        }

        // 被领取了
        var a = textContains("您来晚了").exists();
        var b = textContains("餐品已取消分享").exists();
        if (a || b) {
            console.error("【被抢了！！】");
            console.timeEnd('耗时')
            // 点击返回
            desc('返回').findOne().parent().click();
            continue;
        };
 
        var c = text("立即领取").exists();
        if (c) {
            text("立即领取").click();
            console.info("【领取到一份！！】")
            console.timeEnd('耗时')
            receive_num += 1;
            sleep(500);
            desc('返回').findOne().parent().click();
            continue;
        };

        var d = text('餐品已领取成功 点击查看').exists();
        if (d){
            console.error("【已经领取过了！】");
            console.timeEnd('耗时')
            // 点击返回
            desc('返回').findOne().parent().click();
            continue;
        }


        if (desc('返回').exists()) {
            desc('返回').findOne().parent().click();
        };
    };
};

function main() {
    device.keepScreenOn(); // 保持屏幕常亮 
    dialogs.alert("【欢迎使用自动抢餐脚本v1.3.1】\n节约粮食！！浪费可耻！！\n请为Auto.js开启【悬浮窗】、【无障碍功能】和【稳定模式】\n作者：fxhaoo\n更新日期：2020.11.16")
    console.show();
    // 手机高
    var height = device.height; 
    // 手机宽度
    var width = device.width;
    // 设置手机屏幕
    setScreenMetrics(width, height);
    OpenPermissions();
    selectfoodnum();
    console.info("需要抢餐的数量为：%d", food_num_select)
    foodnameinput();
    // startapp();
    dialogs.alert("脚本正在运行...\n请手动打开微信并进入大群~")
    do {
        if (receive_num == food_num_select) {
            console.info("【已完成领取任务！】");
            console.info("【脚本将自动关闭~】");
            console.info("【欢迎再次使用~】");
            device.vibrate(2000); // 震动提醒脚本执行结束
            engines.stopAll(); // 结束脚本
        }
        food();
    } while (true);
};
main();
