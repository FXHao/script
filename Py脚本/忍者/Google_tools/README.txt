【Install_aab.py】
功能：
    1、读取输出aab文件信息
    2、将aab文件转为apks文件
    3、将apks文件安装到测试设备

使用方法：
    1、装有python环境、配有Java环境
    2、将aab文件放置在当前目录
    3、将签名文件放置在当前目录，且在keystore.ini中配置签名密码和签名别名（当前已配好）
    4、电脑链接好测试设备（开发者模式等需开好，可用adb devices 测试是否链接正常）
    3、在CMD打开当前目录，python Install_aab.py
    4、静待转换安装完成

注意：
    1、测试设备请提前卸载相应的包体
    2、转换过程有点久，若无输出报错信息，静待即可
    3、output.apks为转换后可安装的apk集，若需测试卸载安装、覆盖安装无需重新进行转换
         执行 "java -jar bundletool-all-1.7.0.jar install-apks --apks=output.apks" 即可

【Write_aabInfo.py】
功能：
    1、将aab文件信息写入cvs文件，便于提审

使用方法：
    1、装有python环境
    2、将aab文件放置在当前目录
    3、在CMD打开当前目录，python Write_aabInfo.py
