# -*- coding: utf-8 -*-
import configparser
import os
import re
import subprocess

path = os.path.abspath(os.path.dirname(__file__))

class Google_Tools():

    def __init__(self):
        self.bundletool_path = os.path.join(path, r"bundletool-all-1.7.0.jar")
        self.keystore_path = os.path.join(path, r'android.keystore')
        self.apks_path = os.path.join(path, r'output.apks')

    def get_keystore(self):
        config = configparser.ConfigParser()
        config.read('keystore.ini', encoding='GB18030')
        KeyStorePassword = config.get('JK','password')
        KeyStoreAlias = config.get('JK','alias')
        return KeyStorePassword, KeyStoreAlias

    def get_aabINFO(self, aab):
        input_command = "java -jar {} dump manifest --bundle {}".format(self.bundletool_path, aab)
        info_output = subprocess.Popen(input_command, shell=True, stdout=subprocess.PIPE)
        info = info_output.stdout.read().decode('utf8', 'ignore')
        data = re.search('<manifest xmlns:android="http://schemas.android.com/apk/res/android" android:compileSdkVersion="(\S+)" android:compileSdkVersionCodename="(\S+)" android:versionCode="(\S+)" android:versionName="(\S+)" package="(\S+)" platformBuildVersionCode="(\S+)" platformBuildVersionName="(\S+)">', info)
        versionName = data.group(4)
        versionCode = data.group(3)
        packageName = data.group(5)
        targetSdkVersion = os.popen("java -jar {} dump manifest --bundle {} --xpath //uses-sdk/@android:targetSdkVersion".format(self.bundletool_path, aab)).read()
        minSdkVersion = os.popen("java -jar {} dump manifest --bundle {} --xpath //uses-sdk/@android:minSdkVersion".format(self.bundletool_path, aab)).read()
        print("""
************************* AAB包体信息 ************************
* packageName :    【{}】  
* versionCode :    【{}】  
* versionName :    【{}】  
* targetSdkVersion:【{}】  
* minSdkVersion :  【{}】  
*************************************************************
        """.format(packageName, versionCode, versionName, int(targetSdkVersion), int(minSdkVersion)))


    def aabToapks(self, aab):
        '''
        将aab文件签名并转换为apks文件
        :param aab: 需传入的aab文件
        :return:
        '''
        KS_password, KS_alias= self.get_keystore()
        input_command = "java -jar {} build-apks --bundle={} --output={} --local-testing --ks={} --ks-pass={} --ks-key-alias={}".format(self.bundletool_path, aab, self.apks_path, self.keystore_path, KS_password, KS_alias)
        os.system(input_command)


    def installApks(self):
        input_command = "java -jar {} install-apks --apks={}".format(self.bundletool_path, self.apks_path)
        os.system(input_command)

    def run_main(self):
        aab_tag = 0
        for i in os.listdir(path):
            if os.path.splitext(i)[1] != ".aab":
                continue
            aab_tag = 1
            print("获取到aab文件，开始读取信息...")
            self.get_aabINFO(i)
            print('\t开始将aab文件转化为apks....请耐心等待....')
            self.aabToapks(i)
            print("\t转化完成，正在进行安装....")
            self.installApks()
            print('\t安装完成')
        if aab_tag == 0:
            print('未获取到aab文件，请确认已将aab文件放置在当前目录！')


if __name__ == '__main__':
    a = Google_Tools()
    a.run_main()
    input("按任意键退出~")
