from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

import logging
logger = logging.getLogger()
logger.setLevel(logging.ERROR)

'''
69337700632 小米9
62561099414   oppor15
3526869505018942   oppor-17
71250371748    oppo a8-1
71331543614  vivoz5x-1
71311183116    vivo iqqqneo-2
1151926307396695         安卓9               vivo u33
59711277358        安卓7.1.1   oppoa1 
360292397102701  A11x-2
'''

class Toutiao():

    def __init__(self):
        self.user = 'hzzx01-10@lmadart.com'
        self.pwd = 'Luming@123'
        self.appid = None
        self.did = None
        self.input_did = None
        self.input_appid = None
        self.driver_id_dict = {
            '1': 1151926307396695, # 'vivo-y3'
            '2': 448245920433277,    # P40
            '3': 67043824917,         # vivo-Z3
            '4': 2576890396942888,    # oppo-A11x
            '5': 1151926307396695,  # vivo-U3
            '6': 69337700632,    # 小米9
            '7': 62561099414      # oppo-r15
            '8': 71311183116
        }

    def print_open(self):
        data1 = '可手动输入1~7选择测试用的手机，也可直接输入测试手机的driver_id'
        data = '【1】:vivo-y3\n' \
               '【2】:P40\n' \
               '【3】:vivo-z3\n' \
               '【4】:oppo-a11x\n' \
               '【5】:vivo-u3\n' \
               '【6】:小米9\n' \
               '【7】:oppo-r15\n'
        print('==================欢迎使用头条上包查询 jio本 v1.0===============')
        print(data1)
        print(data)

    def print_close(self):
        print('==================欢迎再次使用头条上包查询 jio本 v1.0===============')

    def user_input(self):

        for x in range(3):
            self.input_appid = input('请输入游戏的【Appid】：回车键确认\n')
            if self.input_appid != '':
                break
        self.appid = self.input_appid

        for x in range(3):
            self.input_did = input('请输入手机对应【编号】或【driver_id】：回车键确认\n')
            if self.input_did != '':
                break

        for key in self.driver_id_dict.keys():
            if self.input_did == key:
                self.did = self.driver_id_dict[key]
                break
            else:
                self.did = self.input_did
        print('driver_id为【{}】'.format(self.did))

    def check_main(self):

        driver = webdriver.Chrome()
        driver.maximize_window()
        wait = WebDriverWait(driver, 30)
        driver.get("https://ad.oceanengine.com/pages/login/index.html")
        sleep(1)

        # 登录
        accout_loc = (By.XPATH, '//input[@name="account"]')
        passwd_loc = (By.XPATH, '//input[@name="password"]')
        login_btn_loc = (By.XPATH, '//div[contains(@class,"login-button")]')

        wait.until(EC.visibility_of_element_located(accout_loc))

        driver.find_element(*accout_loc).send_keys(self.user)
        driver.find_element(*passwd_loc).send_keys(self.pwd)
        driver.find_element(*login_btn_loc).click()

        # 处理首次登录弹窗引导
        check_loc = (By.XPATH, '//button[contains(@class,"bui-btn bui-btn-info byted-button bui-btn-lg")]')
        wait.until(EC.visibility_of_element_located(check_loc))
        driver.find_element(*check_loc).click()
        sleep(1)

        quit_tip_loc = (By.XPATH, '//div[@class="process-quit"]')
        wait.until(EC.visibility_of_element_located(quit_tip_loc))
        driver.find_element(*quit_tip_loc).click()

        # 鼠标操作，移动到资产
        assets_loc = (By.XPATH, '//a[@data-nav-a-id="property"]')
        wait.until(EC.visibility_of_element_located(assets_loc))
        ele = driver.find_element(*assets_loc)
        ta = ActionChains(driver)
        ta.move_to_element(ele).click(ele)
        ta.perform()

        # 点击检测
        listing_loc = (By.XPATH, '//div[text()="转化跟踪"]')
        wait.until(EC.visibility_of_element_located(listing_loc))
        driver.find_element(*listing_loc).click()

        # 点击跟踪应用
        from_loc = (By.XPATH, '//div[text()="跟踪应用"]')
        wait.until(EC.visibility_of_element_located(from_loc))
        driver.find_element(*from_loc).click()

        # 选择sdk转化
        sdk_loc = (By.XPATH, '//p[text()="使用SDK的应用"]')
        wait.until(EC.visibility_of_element_located(sdk_loc))
        driver.find_element(*sdk_loc).click()

        # 选择应用id,不同的应用id，要手动修改脚本的id值
        id_loc = (By.XPATH, '//p[text()="{}"]'.format(self.appid))
        wait.until(EC.visibility_of_element_located(id_loc))
        driver.find_element(*id_loc).click()

        # 点击数据检测
        data_check_loc = (By.XPATH, '//a[text()="数据检测"]')
        wait.until(EC.visibility_of_element_located(data_check_loc))
        driver.find_element(*data_check_loc).click()

        # 点击前往检测
        go_check_loc = (By.XPATH, '//button[text()="前往检测"]')
        wait.until(EC.visibility_of_element_located(go_check_loc))
        driver.find_element(*go_check_loc).click()
        sleep(5)

        # 句柄切换
        win_hans = driver.window_handles
        driver.switch_to.window(win_hans[1])
        sleep(1)

        # 输入设备id，建议使用vivo y3标准版,如要使用其他设备id，请手动修改脚本
        input_id_loc = (By.XPATH, '//input[@class="ant-input"]')
        wait.until(EC.visibility_of_element_located(input_id_loc))
        driver.find_element(*input_id_loc).send_keys(self.did)

        # 点击测试
        test_loc = (By.XPATH, '//button[@class="ant-btn ant-btn-primary"]')
        wait.until(EC.visibility_of_element_located(test_loc))
        driver.find_element(*test_loc).click()


if __name__ == '__main__':
    tt = Toutiao()
    tt.print_open()
    tt.user_input()
    tt.check_main()