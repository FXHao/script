import tkinter as tk
from  tkinter import filedialog
from tkinter import scrolledtext


class Tools_UI():
    def __init__(self):
        self.mode = 0 # 默认0（测试）
        self.path_ = None
        self.root = tk.Tk()
        self.root.title('测试')
        ws = self.root.winfo_screenwidth()
        hs = self.root.winfo_screenheight()
        self.root.geometry('530x500+%d+%d' % ((ws/2)-530/2, (hs/2)-600/2))
        self.root.configure(background="pink")

        # 路径获取
        La1= tk.Label(self.root, text='选择路径:', font=('微软雅黑', 12), background="pink")
        La1.place(x=10, y=10,width=100, height=25)
        self.path = tk.StringVar()

        #输入框绑定变量path
        En1 = tk.Entry(self.root, textvariable = self.path)
        En1.place(x=120, y=10, width=300, height=25)
        B1 = tk.Button(self.root, text="选择", command=self.selectpath, font=('微软雅黑', 11), background="pink")
        B1.place(x=430, y=10, width=60, height=25)
        # 功能复选
        La2 = tk.Label(self.root, text='模式选择:', font=('微软雅黑', 12), background="pink")
        La2.place(x=10, y=55, width=100, height=25)

        self.CheckVar1 = tk.IntVar()
        # C1 = Checkbutton(self.root, text = "测试(仅检测)", variable = CheckVar1,
        #                  onvalue = 1, offvalue = 0, height=-1, width = -1, background="pink")
        # C2 = Checkbutton(self.root, text = "提审(检测+写入表格)", variable = CheckVar2,
        #                  onvalue = 1, offvalue = 0, height=-1, width = -1, background="pink")
        R1 = tk.Radiobutton(self.root,text='测试(仅检测)',variable=self.CheckVar1, value=0, command=self.selection, background="pink")
        R2 = tk.Radiobutton(self.root,text='提审(检测+写入表格)',variable=self.CheckVar1, value=1, command=self.selection,background="pink")
        R1.place(x=20, y=95)
        R2.place(x=150, y=95)

        La3 = tk.Label(self.root, text='结果显示:', font=('微软雅黑', 12), background="pink")
        La3.place(x=10, y=140)

        # 滚动条文本框
        self.Text1 = tk.scrolledtext.ScrolledText(self.root, background="pink", relief="solid", font=('微软雅黑', 10))
        # self.Text1 = tk.Text(self.root, background="pink", font=('微软雅黑', 12))
        # self.Text1.place(x=10, y=180, height=300, width=500)
        self.Text1.place(x=10, y=180, height=300, width=500)

        # 滚动条
        # scroll = tk.Scrollbar()
        # scroll.pack(side=tk.RIGHT,fill=tk.Y)
        # scroll.config(command=self.Text1.yview)
        # self.Text1.config(yscrollcommand=scroll.set)

        B2 = tk.Button(self.root, text='运 行', font=('微软雅黑', 16), command=self.outprint,background='#32CD32')
        B2.place(x=400, y=70, height=80, width=80)


        self.root.mainloop()


    def selectpath(self):
        self.path_ = filedialog.askdirectory()
        # path_ = path_.replace('/', '\\\\')
        self.path.set(self.path_)

    def selection(self):
        self.mode = self.CheckVar1.get()


    def outprint(self):
        if self.mode == 0:
            m = '测试模式'
        elif self.mode == 1:
            m = '提审模式'
        path = '文件路径：%s\n' % self.path_
        mode = '执行模式：%s\n' % m
        datatext = path + mode
        self.Text1.delete(1.0,'end')
        self.Text1.insert('insert', datatext)
        # self.Text1.insert('insert', self.path)




a = Tools_UI()
