# -*- coding: utf-8 -*-
import subprocess
import re
import os
import zipfile


def getAppBaseInfo(parm_apk_path):
    '''
    获取apk信息
    :param parm_apk_path: apk文件
    :return: packagename, versionCode, versionName
    '''
    path = os.path.abspath(os.path.dirname(__file__)) + "//"
    # print(path)
    # apk_path =path + parm_apk_path
    apk_path = parm_apk_path
    # aapt_path = path + "aapt.exe"
    aapt_path = 'D://Users//Desktop//aapt.exe'

    # get_info_command = "%s dump badging %s" % (aapt_path, apk_path)  # 使用命令获取版本信息
    get_info_command = "%s d badging %s" % (aapt_path, apk_path)
    output = subprocess.Popen(get_info_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    data = output.stdout.read().decode('utf8', 'ignore')
    # (output, err) = output.communicate()
    # t = output.decode().split("\n")
    # print(data)

    match = re.compile("package: name='(\S+)' versionCode='(\d+)' versionName='(\S+)' compileSdkVersion='(\d+)'").match(data)
    if match is not None:
        return match.group(4)
    else:
        return None

    # permission_data = output.stdout.read().decode('utf8', 'ignore')
    # permission_data_list = permission_data.split('\n')
    # permission_list = []
    # for i in permission_data_list:
    #     if i is '':
    #         continue
    #     # print(i.split()[1])
    #     permission_list.append(i.split()[1])
    # print('com.track.sdk.ui.activity.PayActivity' in permission_list)
    # print(permission_list)

# getAppBaseInfo('MyTalkingTomFriends-huawei-debug-blocal-v1.1.2.0.1.apk')

"""
    android.permission.SEND_SMS
    android.permission.ACCESS_FINE_LOCATION
    android.permission.CALL_PHONE
"""

class Properties:
    '''解析Properties文件'''
    def __init__(self, file_name):
        self.file_name = file_name
        self.properties = {}
        if os.path.exists(file_name):
            with open(file_name) as f:
                for line in f:
                    tline = line.strip()
                    if tline.startswith('#'):
                        continue
                    else:
                        kv_list = tline.split('=', 2)
                        if not kv_list or len(kv_list) != 2:
                            continue
                        else:
                            value_list = kv_list[1].strip().split(',')
                            if not value_list:
                                continue
                            else:
                                if len(value_list) == 1:
                                    self.properties[kv_list[0].strip()] = value_list[0].strip()
                                else:
                                    temp = []
                                    for v in value_list:
                                        temp.append(v.strip())
                                    self.properties[kv_list[0].strip()] = temp
        else:
            raise Exception("file %s not found" % file_name)

    def get(self, key):
        if key in self.properties:
            return self.properties[key]
        return ''

    def get_list(self, key):
        if key in self.properties:
            temp = self.properties[key]
            if isinstance(temp, list):
                return temp
            else:
                return [temp]
        return []

    def get_num(self, key):
        if key in self.properties:
            return float(self.properties[key])
        return 0



def get_TrackSdkConfig(key):
    with zipfile.ZipFile('TalkingTomPool-mateapp-jiumengad-rc-b2-v2.0.7.2.apk','r') as apkzip:
        data = apkzip.read('assets/TrackSdkConfig.properties')
        data = str(data, encoding='gbk')
        properties_dict = {}
        line_list = data.split('\n')

        for line in line_list:
            line.strip()
            if line.startswith('#') or (line is '\r') or (line is ''):
                continue
            # print(line)
            kv = line.split('=')
            if len(kv) < 2:
                continue
            k = kv[0].strip()
            v = kv[1].strip()
            properties_dict[k] = v
    print(properties_dict)
    if key in properties_dict:
        return properties_dict[key]
    return "未找到{}".format(key)



    # if key in properties:
    #     return properties[key]
    # return '111'



def get_properties(key):
    file_name = '../TrackSdkConfig.properties'
    properties = {}
    if os.path.exists(file_name):
        with open(file_name) as f:
            for line in f:
                tline = line.strip()
                if tline.startswith('#'):
                    continue
                else:
                    kv_list = tline.split('=', 2)
                    if not kv_list or len(kv_list) != 2:
                        continue
                    else:
                        value_list = kv_list[1].strip().split(',')
                        if not value_list:
                            continue
                        else:
                            if len(value_list) == 1:
                                properties[kv_list[0].strip()] = value_list[0].strip()
                            else:
                                temp = []
                                for v in value_list:
                                    temp.append(v.strip())
                                properties[kv_list[0].strip()] = temp
    else:
        raise Exception("file not found")

    if key in properties:
        return properties[key]
    return ''

# get_TrackSdkConfig()
# a = Properties("TrackSdkConfig.properties")
# print(a.get('app_id'))
# print(get_properties('app_id'))
# print(get_TrackSdkConfig('app_id'))
# print(get_TrackSdkConfig('app_id'))
# print(getAppBaseInfo('TalkingTomPool-mateapp-jiumengad-rc-b2-v2.0.7.2.apk') == '汤姆猫水上乐园')
# a = getAppBaseInfo('MyTalkingTomFriends-cps-debug-blocal-v1.5.1.0.apk')
# print(a)

class A():
    def __init__(self):
        self.a = 1

c = A()
print(c.a)