# -*- encoding=utf8 -*-
"""
v = 1.3
date = 2020-12-12
"""

import hashlib
import os
import re
import subprocess
import zipfile
import time
from lxml import etree


#[渠道, 包名, ]
channerdict = {
    'jinke':['', None],
    'm4399': ['4399', 'com.jinke.rzkp.m4399'],
    'vivosingle': ['vivo', 'com.jinke.rzkp.vivo'],
    'TapTap': ['TapTap', 'com.jinke.rzkp.cps'],
    'google': ['google', 'com.jk.and.ninja']
}

#[游戏]
gamenamedict = {
    'renzhekuaipao': ['忍者跑酷']
}

def getApkBaseInfo(parm_apk_path):
    '''
    获取apk包名，版号，内部版本号，权限信息
    :param parm_apk_path: apk文件
    :return: packagename, versionCode, versionName
    '''
    path = os.path.abspath(os.path.dirname(__file__)) + "//"
    # print(path)
    apk_path =path + parm_apk_path
    aapt_path = 'D://Users//Desktop//aapt.exe'

    # 使用命令获取版本信息
    get_info_command = "%s dump badging %s" % (aapt_path, apk_path)
    # get_info_command = "%s l %s" % (aapt_path, apk_path)
    info_output = subprocess.Popen(get_info_command, shell=True, stdout=subprocess.PIPE)
    info = info_output.stdout.read().decode('utf8', 'ignore')

    # 通过正则匹配，获取包名，版本号，版本名称
    match = re.compile("package: name='(\S+)' versionCode='(\d+)' versionName='(\S+)'").match(info)
    if not match:
        raise Exception("{}找不到apk版号信息~".format(parm_apk_path))
    
    g_name = re.search("application-label-zh:'(\S+)'", info)
    # print(g_name is None)
    if g_name is None:
        g_name = re.search("application-label-zh-CN:'(\S+)'", info)
    elif g_name is None:
        g_name = re.search("application-label:'(\S+)'", info)
    elif g_name is None:
        g_name = re.search("application-label-en:'(\S+)'", info)
    elif g_name is None:
        raise Exception("{}找不到apk中文名信息~".format(parm_apk_path))
    packagename = match.group(1)
    versionCode = match.group(2)
    versionName = match.group(3)


    # 获取权限目录
    get_permission = "%s d permissions %s" % (aapt_path, apk_path)
    p_output = subprocess.Popen(get_permission, shell=True, stdout=subprocess.PIPE)
    p_data = p_output.stdout.read().decode('utf8', 'ignore')
    p_data_list = p_data.split('\n')
    permission_list = []
    for i in p_data_list:
        if i is '':
            continue
        permission_list.append(i.split()[1])

    # return packagename, versionCode, versionName, permission_list, apk_game_name
    return packagename, versionCode, versionName, permission_list

def getabbBaseInfo(parm_apk_path):
    path = os.path.abspath(os.path.dirname(__file__)) + "//"
    # print(path)
    aab_path =path + parm_apk_path
    bundletool_path = 'D://Users//Desktop/bundletool-all-1.7.0.jar'
    cmdinput = "java -jar %s dump manifest --bundle %s" % (bundletool_path, aab_path)
    info_output = subprocess.Popen(cmdinput, shell=True, stdout=subprocess.PIPE)
    info = info_output.stdout.read().decode('utf8', 'ignore')
    # print(info)
    tree = etree.XML(info)
    # packagename = tree.xpath(r'//manifest/@package')[0]
    # versionCode = tree.xpath(r'//manifest/@android:versionCode')
    # versionName = tree.xpath(r'//manifest/@android:versionName')
    minSdkVersion = tree.xpath('r//uses-sdk/@android:minSdkVersion')
    # print(packagename)
    # print(versionCode)
    # print(versionName)
    print(minSdkVersion)




def get_TrackSdkConfig(key, apk_path):
    '''
    获取TrackSdk配置信息
    :param key: 需获取的参数
    :return: 获取参数的值
    '''
    path = os.path.abspath(os.path.dirname(__file__)) + "//"
    # print(path)
    apk_path =path + apk_path
    with zipfile.ZipFile(apk_path, 'r') as apkzip:
        data = apkzip.read('assets/TrackSdkConfig.properties')
        try:
            data = str(data, encoding='gbk')
        except Exception as e:
            data = str(data, encoding='utf8')
        
        properties_dict = {}
        line_list = data.split('\n')

        for line in line_list:
            if line.startswith('#') or (line is '\r') or (line is ''):
                continue
            # k, v = line.split('=')
            kv = line.split('=')
            if len(kv) < 2:
                continue
            k = kv[0].strip()
            v = kv[1].strip()

            properties_dict[k] = v
    # print(properties_dict)
    if key in properties_dict:
        return properties_dict[key]
    return "未找到{}".format(key)


def apkcheck(filename):
    '''
    获取apk文件信息，游戏名和渠道是根据文件名，版本信息是根据apk文件信息获取
    '''
    print("========================== 开始获取、读写apk文件信息...... =======================")
    num_1 = 0
    num_2 = 0
    with open(os.path.join("getmd5.csv"), "w") as csv:
        csv.write("{0},{1},{2},{3},{4},{5},{6},{7}\n".format("游戏", "更新内容", "渠道", "版本", "内部版本号", "文件名", "包名","MD5"))
        for i in os.listdir(filename):
            if os.path.splitext(i)[1] != (".apk" or ".aab"):
                continue

            file = open(os.path.join(filename, i), 'rb')
            md5 = hashlib.md5(file.read()).hexdigest()
            gamename = i.split("-")[0]
            channel = i.split("-")[1]
            packagename, versionCode, versionName, permission_list = getApkBaseInfo(i)
            app_id = gamenamedict[gamename][2]
            ganme_name = gamenamedict[gamename][3]
            csv.write("{0},{1},{2},{3},{4},{5},{6},{7}\n".format(gamenamedict[gamename][0], "", channerdict[channel][0],
                                                         versionName, versionCode, i, packagename, md5))
            print("写入{0} 成功".format(i))
            num_1 += 1

        print("========================== apk文件信息读写完成~ =================================")

if __name__ == "__main__":
    # dir = os.getcwd()
    # apkcheck(dir)
    # print("""
    # *+*+*+*+*+【！！！记得写提包时间！！！】*+*+*+*+*+
    # *+*+*+*+*+【！！！记得写提包时间！！！】*+*+*+*+*+
    # *+*+*+*+*+【！！！记得写提包时间！！！】*+*+*+*+*+
    # """)
    # input('任意按钮关闭')
    getabbBaseInfo("app-google-renzhekuaipaoforeign-unity3dforeign-release.aab")
